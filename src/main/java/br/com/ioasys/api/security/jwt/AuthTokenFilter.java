package br.com.ioasys.api.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ioasys.api.models.User;
import br.com.ioasys.api.repository.UserRepository;
import br.com.ioasys.api.security.services.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.Base64Utils;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;


public class AuthTokenFilter extends OncePerRequestFilter {
	@Autowired
	private JwtUtils jwtUtils;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	PasswordEncoder encoder;

	private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			String jwt = parseJwt(request);
			String headerClient = request.getHeader("client");
			String headerUid = request.getHeader("uid");
			System.out.println(headerClient);
			System.out.println(headerUid);

			if (jwt != null && jwtUtils.validateJwtToken(jwt)) {
				String username = jwtUtils.getUserNameFromJwtToken(jwt);
				String client = Base64Utils.encodeToString(username.getBytes());
				UserDetails userDetails = userDetailsService.loadUserByUsername(username);
				User user = userRepository.findByName(username)
						.orElseThrow(() -> new UsernameNotFoundException("User Not Found with name: " + username));
				System.out.println(username);
				System.out.println(client);
				if (client.equals(headerClient)) {
					if (headerUid.equals(user.getEmail())) {
						UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
								userDetails, null, userDetails.getAuthorities());
						authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

						SecurityContextHolder.getContext().setAuthentication(authentication);
					}
				}
			}
		} catch (Exception e) {
			logger.error("Cannot set user authentication: {}", e);
		}

		filterChain.doFilter(request, response);
	}

	private String parseJwt(HttpServletRequest request) {
		String headerAccessToken = request.getHeader("access-token");
		System.out.println(headerAccessToken);

		if (StringUtils.hasText(headerAccessToken) && headerAccessToken.startsWith("Bearer ")) {
			return headerAccessToken.substring(7, headerAccessToken.length());
		}

		return null;
	}
}
