package br.com.ioasys.api.repository;

import br.com.ioasys.api.models.Enterprise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EnterpriseRepository extends JpaRepository<Enterprise, Long> {

    @Query("SELECT e FROM Enterprise e WHERE e.enterprise_name = ?1")
    Enterprise findByEnterpriseName(String enterpriseName);

    @Query("SELECT e FROM Enterprise e WHERE e.enterprise_name LIKE %?1%")
    Enterprise findByEnterpriseNameLike(String enterpriseName);

}