package br.com.ioasys.api.controllers;

import br.com.ioasys.api.models.Enterprise;
import br.com.ioasys.api.models.User;
import br.com.ioasys.api.payload.request.EnterpriseRequest;
import br.com.ioasys.api.payload.response.MessageResponse;
import br.com.ioasys.api.payload.response.UserResponse;
import br.com.ioasys.api.repository.EnterpriseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
@RequestMapping(value = "${empresas.app.url.enterprises}")
public class EnterpriseController {

	@Autowired
	EnterpriseRepository enterpriseRepository;

	private static final String ENTERPRISE_NAME = "AllRide";
	@GetMapping(value = "")
	public ResponseEntity<?> getAll(@RequestParam(name = "name") String name) {
		List<Enterprise> enterprises = new ArrayList<>();
		Enterprise enterprise;
		if (name != null) {
			enterprise = enterpriseRepository.findByEnterpriseNameLike(name);
			enterprises.add(enterprise);
		} else {
			enterprise = new Enterprise();
			enterprises = enterpriseRepository.findAll();
			if (enterprises.size() == 0) {
				enterprise.setEnterprise_name(ENTERPRISE_NAME);
				enterprise.setDescription("Urbanatika is a socio-environmental company with economic impact, creator of the agro-urban industry. We want to involve people in the processes of healthy eating, recycling and reuse of organic waste and the creation of citizen green areas. With this we are creating smarter cities from the people and at the same time the forest city. Urbanatika, Agro-Urban Industry");

				Enterprise enterpriseExist = enterpriseRepository.findByEnterpriseName(enterprise.getEnterprise_name());
				if (enterpriseExist == null) {
					enterpriseRepository.save(enterprise);
					enterprises.add(enterprise);
				} else {
					enterprises.add(enterpriseExist);
				}
			}
		}


		return ResponseEntity.ok(enterprises);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<?> getEnterpriseById(@PathVariable Long id) {
		try {
			Optional<Enterprise> enterprise = enterpriseRepository.findById(id);
			return ResponseEntity.ok(enterprise);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ResponseEntity.notFound().build();

	}

	@PostMapping("")
	public ResponseEntity<?> registerEnterprise(@Valid @RequestBody EnterpriseRequest enterpriseRequest) {
		Enterprise enterprise = new Enterprise();
		enterprise.setEnterprise_name(enterpriseRequest.getEnterprise_name());
		enterprise.setDescription(enterpriseRequest.getDescription());

		enterpriseRepository.save(enterprise);

		return ResponseEntity.ok(enterprise);
	}

}
