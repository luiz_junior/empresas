package br.com.ioasys.api.controllers;

import javax.validation.Valid;

import br.com.ioasys.api.models.User;
import br.com.ioasys.api.payload.request.LoginRequest;
import br.com.ioasys.api.payload.request.SignupRequest;
import br.com.ioasys.api.payload.response.JwtResponse;
import br.com.ioasys.api.payload.response.MessageResponse;
import br.com.ioasys.api.repository.UserRepository;
import br.com.ioasys.api.security.jwt.JwtUtils;
import br.com.ioasys.api.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import java.util.Base64;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
@RequestMapping(value = "${empresas.app.url.users.auth}")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/sign_in")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		User user = userRepository.findByEmail(loginRequest.getEmail());
		if (user == null) {
			user = new User("teste",
					"testeapple@ioasys.com.br",
					encoder.encode("12341234"));

			userRepository.save(user);
		}
		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(user.getName(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		JwtResponse jwtResponse = new JwtResponse(jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail());
		String client = Base64Utils.encodeToString(userDetails.getUsername().getBytes());
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("access-token", jwtResponse.getAccessToken());
		responseHeaders.set("client", client);
		responseHeaders.set("uid", userDetails.getEmail());
		responseHeaders.set("token-type", jwtResponse.getTokenType());
		System.out.println("client: ".concat(userDetails.getUsername().concat(" => ").concat(client)));
		return new ResponseEntity(jwtResponse.toString(), responseHeaders, HttpStatus.OK);
	}

	@PostMapping("/sign_up")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByName(signUpRequest.getName())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}

		// Create new user's account
		User user = new User(signUpRequest.getName(),
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
