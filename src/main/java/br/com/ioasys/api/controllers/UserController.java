package br.com.ioasys.api.controllers;

import br.com.ioasys.api.models.User;
import br.com.ioasys.api.payload.response.UserResponse;
import br.com.ioasys.api.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@PropertySource(value = "classpath:/application.properties", ignoreResourceNotFound = true)
@RequestMapping(value = "${empresas.app.url.users}")
public class UserController {

	@Autowired
	UserRepository userRepository;

	@GetMapping(value = "/{name}")
	public ResponseEntity<?> getUserByName(@PathVariable String name) throws UsernameNotFoundException {
		User user = userRepository.findByName(name)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found with name: " + name));
		return ResponseEntity.ok(new UserResponse(user.getId(), user.getName(), user.getEmail()));
	}
}
